const CustomerController = require('../controllers').customers;
const DriverController = require('../controllers').drivers;
const RideController = require('../controllers').ride;

module.exports = (app) => {

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-LoginToken, X-UserId,Version, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");

    next();
  });

  app.get('/', (req, res) => res.status(200).send({
    message: 'Welcome to the Rapido API!',
  }));

  app.get('/fare/:pick_up_latitude/:pick_up_longitude/:drop_latitude/:drop_longitude', RideController.getFare);
  app.get('/surge/:clusterId', RideController.getSurge);
  app.get('/cluster/:latitude/:longitude',RideController.getCluster);
};
