module.exports = (sequelize, DataTypes) => {
  const drivers = sequelize.define('drivers', {
    latitude: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    longitude: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    cluster_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  },{
    timestamps: false
  });
  return drivers;
};
