const ErrorMap = (error) => {
    return { success: false, data: error } // we can add a JSON error mapping if necessary
}

const SuccessMap = (data) => {
    return { success: true, data: data }
}


exports.ErrorMap = ErrorMap;
exports.SuccessMap = SuccessMap;
