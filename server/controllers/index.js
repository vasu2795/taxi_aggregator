const customers = require('./customers');
const drivers = require('./drivers');
const ride = require('./ride');

module.exports = {
  customers,
  drivers,
  ride
};
