const customers = require('../models').customers;
const drivers = require('../models').drivers;
const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const distance = require('google-distance');
const geohash = require('ngeohash');
const computeFare = (distance,time) => {
  return (distance * 5 + time * 1 + 15).toFixed(2); // fetch factors from config instead of hardcoding
}

const computeSurge = (customer_count,driver_count) => {
  let ratio = parseFloat(customer_count)/driver_count;
  if(ratio<1)                         // fetch surge factor and range from config file instead of hardcoding
    return 1;
  else if(ratio < 1.5)
    return 1.2;
  else if(ratio < 2.5)
    return 1.5;
  else
    return 2
}

module.exports = {
  getCluster(req,res) {
    let hash =  geohash.encode(req.params.latitude,req.params.longitude);
    if(! (hash.indexOf('tdr1') == 0) )
      return res.status(201).send(ErrorMap("Invalid Latitude and Longitude - Not in Bangalore"));
    else {
      let cluster = Math.ceil((parseInt(hash.substring(4,6), 36) + 1)/12.96)
      res.status(201).send(SuccessMap("Cluster id is " + cluster));
    }
  },
  getFare(req,res){
    let fromhash =  geohash.encode(req.params.pick_up_latitude,req.params.pick_up_longitude);
    let tohash =  geohash.encode(req.params.drop_latitude,req.params.drop_longitude);

    if(!(fromhash.indexOf('tdr1') == 0) || !(tohash.indexOf('tdr1') == 0))
      return res.status(201).send(ErrorMap("Invalid Latitude and Longitude - Not in Bangalore"));

    distance.get(
    {
      index: 1,
      origin: req.params.pick_up_latitude + ',' + req.params.pick_up_longitude,
      destination: req.params.drop_latitude + ',' + req.params.drop_longitude
    },
    function(err, data) {
      if (err) return res.status(201).send(ErrorMap("Error computing fare"));
      console.log(data);
      return res.status(201).send(SuccessMap("Rs " + computeFare(data.distanceValue/1000,data.durationValue/60)));
    });
  },
  getSurge(req,res){
    let customer_count = 0;
    let driver_count = 0;

    if(req.params.clusterId<1 || req.params.clusterId>100 )
      return res.status(201).send(ErrorMap("Invalid Cluster Id"));

    drivers.count({ where: {'cluster_id': req.params.clusterId}}).then((response) => {
      driver_count = response;
      if(driver_count == 0) {
        return res.status(201).send(ErrorMap("No drivers available"));
      }
      else {
        customers.count({ where: {'cluster_id': req.params.clusterId}}).then((response) => {
          customer_count = response;
          return res.status(201).send(SuccessMap(computeSurge(customer_count,driver_count)));
        }).catch((error) => {
          res.status(201).send(ErrorMap("Error getting customers data"));
        })
      }
    }).catch((error) => {
      res.status(201).send(ErrorMap("Error getting drivers data"));
    })
  }
};
