const customers = require('../models').customers;
const drivers = require('../models').drivers;
const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const geohash = require('ngeohash');

module.exports = {
  bulkupdate(req,res){
    res.status(201).send(SuccessMap("created "))
  },
  create(lat,lng) {
    let hash =  geohash.encode(lat,lng);
    let cluster_id = null;
    if(hash.indexOf('tdr1') == 0) {
      cluster_id = Math.ceil((parseInt(hash.substring(4,6), 36) + 1)/12.96)
    }

    customers.create({
      latitude: lat, longitude: lng, cluster_id: cluster_id
    })
  },
  delete(lat,lng) {
      customers.find({
        where: {
          latitude: lat, longitude: lng
        }
      }).then((customer) => {
        customer.destroy();
      }).catch((error) => {
        console.log("error",error);
      })
  }
};
